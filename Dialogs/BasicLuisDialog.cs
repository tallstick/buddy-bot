using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using LuisBot;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
 
namespace Microsoft.Bot.Sample.LuisBot
{
    // For more information about this template visit http://aka.ms/azurebots-csharp-luis
    [Serializable]
    public class BasicLuisDialog : LuisDialog<object>
    {
        public BasicLuisDialog() : base(new LuisService(new LuisModelAttribute(
            "38f0670d-781a-4134-93eb-59fe6f65a78a", 
            "21d91337916747e48c732b0366ac936b", 
            domain: "westeurope.api.cognitive.microsoft.com")))
        {
        }

        [LuisIntent("None")]
        public async Task NoneIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("I didn't quite get that. Please try again");
        }

        // Go to https://luis.ai and create a new intent, then train/publish your luis app.
        // Finally replace "Gretting" with the name of your newly created intent in the following handler
        [LuisIntent("Greeting")]
        public async Task GreetingIntent(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Hi Jimmy :)");
        }

        [LuisIntent("FeelingUpdate")]
        public async Task FeelingIntent(IDialogContext context, LuisResult result)
        {
            if (result.Query.Contains("sad"))
            {
                await context.PostAsync("It's ok to be sad, take a minute to think about something that makes you happy?");

            }
            else if (result.Query.Contains("angry"))
            {
                await context.PostAsync("We all get angry sometimes, it's important to take time to relax");
            }
            else if (result.Query.Contains("happy") || result.Query.Contains("happier"))
            {
                await context.PostAsync("that's great, when you are happy I am also happy :)");
            }
        }

        [LuisIntent("Solution")]
        public async Task SolutionIntent(IDialogContext context, LuisResult result)
        {

            if (result.Query.Contains("mum"))
            {
                SmsSender smsSender = new SmsSender();
                smsSender.SendSms();
                await context.PostAsync("I have alerted her, she will be with you any second");
            }
            else if (result.Query.Contains("youtube"))
            {
                await context.PostAsync("Consider it done :)");
                System.Diagnostics.Process.Start("http://youtube.com");
            }
            else if (result.Query.Contains("favourite"))
            {
                await context.PostAsync("Consider it done :)");

                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=-M00MS90pzQ");
            }
            else if (result.Query.Contains("game"))
            {
                await context.PostAsync("Sure thing :)");

                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=A1VXLx-9IOE");
            }
            else if (result.Query.Contains("ocean"))
            {
                await context.PostAsync("Enjoy :)");
                System.Diagnostics.Process.Start(@"C:\Users\ahelmahd\Downloads\Ocean_Waves-Mike_Koenig-980635527.wav");
            }
            else
            {
                await context.PostAsync($"I can do that for you. Give me a second");
            }
        }

        private async Task ShowLuisResult(IDialogContext context, LuisResult result) 
        {
            await context.PostAsync($"You have reached {result.Intents[0].Intent}. You said: {result.Query}");
            context.Wait(MessageReceived);
        }
    }
}